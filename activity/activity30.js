
//count the total number of fruits on sale.

db.fruits.aggregate([
	{$match: {onSale: true}}, 
	{$group: {_id: "$_id"}},
    {$count: "fruitsOnSale"}
]);


// more than 20

db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}}, 
	{$group: {_id: "$_id"}},
    {$count: "enoughStock"},
	{$project: {_id: 0}}
        
]);


// Aggregation to get the average price of fruits onSale per supplier


db.fruits.aggregate([
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
        
]);


// the highest price of a fruit per supplier

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
        {$sort: {max_price: -1}}
        
]);


// lowest price of a fruit per supplier.

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", min_prince: {$min: "$price"}}},
        {$sort: {min_prince: 1}}
        
]);