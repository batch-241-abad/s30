/*
	MONGODB: aggregation and query cases studies


	-mongodb aggregation is used to generate manipulated data and perform operations to create filter results and helps in analyzing data.

	-compared to doing crud operations, aggregation gives us access to manipulate, filter and compute for results. Providing us with information to make necessary development wihtout having to create a frontend application.

	

	USING THE AGGREGATED METHOD

	the "$match" is used to pass the document that meet the specified condition(s) to the next pipeline stage or aggregation process


	SYNTAX:

		- { $match: {field: value}}
		- { $group: {_id: "value", fieldResult: "valueResult"}}
	
	- USING both "$match" and "$group" along with aggregation will find for products that are on sale and awill group all stocks for all supliers found.

		db.collectionName.aggregate([
			{$match {fieldA: valueA}},
			{$group: {_id: "$fieldB"}, {result: {operation}}
		])

	- the "$" symbol will refer to a field name that is available the documents that are being aggregated on
	- the "$sum" operator will total the value of all "stock" field and returned documents that are found using the "$match" criteria
*/


db.fruits.aggregate([
	{$match: {onSale: true}}, 
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);



/*
	FIELD PROJECTION WITH AGGREGATION

	the "$project" can be used when aggretating data to include or exclude fields from the returned results.

	SYNTAX:

		{$project: {field 1/ 0}}

*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$project: {_id: 0}}
	])


/*
	SORTING AGGREGATED RESULT

	-the "$sort" can be used to change the order of aggregated results.

	- providing a value of -1 will sort the aggregated results in a reverse order

	SYNTAX

		{$sort: {field: 1 / -1}}


*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort: {total: -1}}

	]);


/*
	AGGREGATING RESULTS BASED ON ARRAY FIELDS

	-THE "$unwind" deconstructs an array field from a collection or field with an array value to output a reulst for each element
	-the syntax below reeturn results, creating seperate documents for each of the countries provided per the "origin" field

	SYNTAX:
		- {$unwind: "$field"}

*/

db.fruits.aggregate([
	{$unwind: "$origin"}
	]);

// display fruits documents by their origin and the kind of fruits that are supplied

db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id: "$origin", kinds: {$sum: 1}}}
])